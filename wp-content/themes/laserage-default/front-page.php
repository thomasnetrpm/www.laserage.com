<?php

	/*
		Template Name: Front Page
	*/
?>
<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/html-header', 'parts/shared/header' ) ); ?>

<!--Site Content-->
<section class="site-content" role="main">

<section class="site-intro">
            <div class="inner-wrap">
                <figure class="ametek-logo"><img src="<?php bloginfo('template_url'); ?>/img/logo-ametech.png" alt="A Business Unit of AMETEK"></figure>
            	<span class="site-tagline">Experts in Precision Laser Component Manufacturing</span>
                <h1 class="site-intro-h1">
                   <span>Laserage is Your Premier Full-Service</span> Laser Processing Company for Precision Tube Cutting, Laser Scribing, Drilling, Welding & Other Custom Services <span>Now a unit of AMETEK's Engineered Medical Components Division.</span>
                </h1>
                <div class="site-intro-ctas">
                   
                   <div class="cta-wrap"><a href="http://laserprocessing.laserage.com/evolution-of-medical-stents-ebook" class="gray-btn ico-download site-intro-cta-1" target="blank"><span><b>Download</b> our eBook:<br>The Evolution of<br>Medical Stents</span></a></div>

                    <!--<div class="cta-wrap"><a href="http://laserprocessing.laserage.com/evolution-of-medical-stents-ebook" class="gray-btn ico-download site-intro-cta-1" target="blank"><span><b>Download</b> our eBook:<br><em>New!</em> The Evolution <br>of Medical Stents</span></a></div> -->

                    <div class="cta-wrap"><a href="http://laserprocessing.laserage.com/a-guide-to-laser-welding-conventional-welding-vs-laser-welding" class="gray-btn ico-download site-intro-cta-2" target="blank"><span><b>Download</b> our eBook: <br>A Guide to Laser Welding:<br>Conventional vs Laser</span></a></div>

                    <!--<div class="cta-wrap"><a href="http://laserprocessing.laserage.com/developing-quality-medical-devices-with-precision-laser-processing-ebook" class="gray-btn ico-download site-intro-cta-2" target="blank"><span><b>Download</b> our eBook: <br>Developing Quality Medical Devices with Precision Laser Processing</span></a></div>-->
                    
                    <div class="cta-wrap resource-library-cta"><a href="http://www.laserage.com/news" class="gray-btn ico-resource-lib site-intro-cta-3" target="blank"><span class="resource-cta">Browse Our</span><span><b>Resources Library</b></span></a></div>

                </div>
            </div>
        </section>
		<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/services-module','parts/shared/about-module','parts/shared/industries-module','parts/shared/social-module' ) ); ?>
        

</section>

<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/footer','parts/shared/html-footer' ) ); ?>