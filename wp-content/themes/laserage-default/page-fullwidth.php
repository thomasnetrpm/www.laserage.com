<?php

	/*
		Template Name: Fullwidth
	*/
?>
 
    
<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/html-header', 'parts/shared/header' ) ); ?>
       
<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

	<!--Site Content-->
	<section class="site-content" role="main">
		<section class="site-intro-dest dark-module">
            <div class="inner-wrap">
            <span class="site-tagline">Experts in Precision Laser Component Manufacturing</span>
            <?php if (is_page( '993' ) || '993' == $post->post_parent) : ?><?php Starkers_Utilities::get_template_parts( array( 'parts/shared/industries-header' ) ); ?><?php endif; ?>
                <h1 class="site-intro-h1">
            	    	<?php the_title(); ?>
                </h1>
            </div>
        </section>
	    <div class="inner-wrap">

	        
	        	
	       		<?php the_content(); ?> 
				<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/flexible-content' ) ); ?>
				<?php if (is_page( '9' )) : ?>
					<!--Sitemap Page-->
				    <ul>
				    <?php
				    // Add pages you'd like to exclude in the exclude here
				    wp_list_pages(
				    array(
				    'exclude' => '',
				    'title_li' => '',
				    )
				    );
				    ?>
				    </ul>
				<?php endif; ?>                    
	       
	        
	        <?php Starkers_Utilities::get_template_parts( array( 'parts/shared/flexible-content-fullwidth' ) ); ?>

			<?php if (is_page( '98209384' )) : ?>
				<!--Products Page-->
				<?php global $query_string; $posts = query_posts('&order=ASC&orderby=menu_order&showposts=20&post_parent=16&post_type=page');?>
				<div class="product-grid">
					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
					<a href="<?php the_permalink(); ?>" class="prd-grd-item">
						<figure class="prd-grd-img">
						<?php the_post_thumbnail('medium'); ?>
						</figure>
						<span class="prd-grd-body">
						<h3 class="prd-grd-header"><?php the_title(); ?></h3>
						</span>
					</a>
					<?php endwhile; ?>
					<?php endif; ?>
				</div>
				<?php wp_reset_query(); ?>
			<?php endif; ?>

		</div>
	</section>

<?php endwhile; ?>

<?php if(get_field('slide_cta') ): ?>
	 <p id="last"></p>
           <div id="slidebox"><a class="close">&nbsp;</a>
          <?php the_field('slide_cta'); ?>
<!-- end HubSpot Call-to-Action Code -->
</div>
		
<?php endif; ?>

<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/social-module','parts/shared/footer','parts/shared/html-footer' ) ); ?>