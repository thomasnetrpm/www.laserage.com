<section class="img-txt-module">
                <div class="inner-wrap">
                    <h2 class="section-header">Industries</h2>
                    <div class="img-txt-grid">
                    <div class="img-txt-item-wrap">
                            
                            <a href="<?php bloginfo('url'); ?>/medical" class="img-txt-item">
                                <figure class="img-txt-figure"><img src="<?php bloginfo('template_url'); ?>/img/ico-medical.svg" alt="Medical Industry"></figure>
                                <h3 class="img-txt-header">Medical</h3>
                                <p class="img-txt-body">
                                    We offer extensive laser processing services for most medical-approved materials, including laser cutting, drilling & welding. Our advanced laser systems deliver exceptionally precise results that meet the most stringent medical requirements. 
                                </p>
                                <span class="img-txt-btn"><b class="yellow-outline-btn">Learn More</b></span>
                            </a>
                        </div>
                    	<div class="img-txt-item-wrap">
                             
                            <a href="<?php bloginfo('url'); ?>/aerospace" class="img-txt-item">
                                <figure class="img-txt-figure"><img src="<?php bloginfo('template_url'); ?>/img/ico-aerospace.svg" alt="Aerospace Industry"></figure>
                                <h3 class="img-txt-header">Aerospace</h3>
                                <p class="img-txt-body">
Laserage has more than 35 years of experience & engineering expertise in the aerospace industry. In addition to laser cutting, drilling & welded assemblies, many of our aerospace clients benefit from customized laser-processed assemblies.                            </p>
                                <span class="img-txt-btn"><b class="yellow-outline-btn">Learn More</b></span>
                            </a>
                        </div>
                        <div class="img-txt-item-wrap">
                           
                            <a href="<?php bloginfo('url'); ?>/industrial" class="img-txt-item">
                                <figure class="img-txt-figure"><img src="<?php bloginfo('template_url'); ?>/img/ico-industrial.svg" alt="Industrial Industry" class="img-txt-image"></figure>
                                <h3 class="img-txt-header">Industrial</h3>
                                <p class="img-txt-body">
Laserage has applied its expertise & state-of-the-art laser systems to develop a variety of industrial applications. Our services have been integral to the creation of many everyday items, including air filters, faucet valves, automotive assemblies & more.
                                </p>
                                <span class="img-txt-btn"><b class="yellow-outline-btn">Learn More</b></span>
                            </a>
                        </div>
                                                
                        <div class="img-txt-item-wrap">
                           
                            <a href="<?php bloginfo('url'); ?>/microelectronics" class="img-txt-item">
                                <figure class="img-txt-figure"><img src="<?php bloginfo('template_url'); ?>/img/ico-microelectronics.svg" alt="Microelectronics Industry" class="img-txt-image"></figure>
                                <h3 class="img-txt-header">Microelectronics</h3>
                                <p class="img-txt-body">
As demand for miniaturized electronic components escalates, our laser micromachining services are being utilized for a growing number of microelectronics applications. </p>
                                <span class="img-txt-btn"><b class="yellow-outline-btn">Learn More</b></span>
                            </a>
                        </div>
                        
                    </div>
                </div>
            </section> <!-- img-txt-module END -->