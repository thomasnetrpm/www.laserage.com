
<!--Site Header-->
<header class="site-header" role="banner">
    <div class="utility-nav">
        <div class="inner-wrap">
             <a href="#" class="site-ph">847-249-5900</a>
                <a href="mailto:Sales@Laserage.com" class="site-email">Sales@Laserage.com</a>

            
           
            <a href="<?php bloginfo('url'); ?>" class="ico-english">English</a>
            <a href="<?php bloginfo('url'); ?>/chinese" class="ico-chinese">Chinese</a>

            <div class="social-wrap tablet">
                <a href="https://www.facebook.com/laseragetechnologycorp" target="_blank"><img src="<?php bloginfo('template_url'); ?>/img/ico-facebook.svg" data-png-fallback="<?php bloginfo('template_url'); ?>/img/ico-facebook.png" alt="Facebook"></a>
                <a href="https://twitter.com/laseragetech" target="_blank"><img src="<?php bloginfo('template_url'); ?>/img/ico-twitter.svg" data-png-fallback="<?php bloginfo('template_url'); ?>/img/ico-twitter.png" alt="Twitter"></a>
                <a href="https://plus.google.com/u/0/+Laserage/posts" target="_blank"><img src="<?php bloginfo('template_url'); ?>/img/ico-googleplus.svg" data-png-fallback="<?php bloginfo('template_url'); ?>/img/ico-googleplus.png" alt="Google Plus"></a>
                <a href="https://www.linkedin.com/company/133282" target="_blank"><img src="<?php bloginfo('template_url'); ?>/img/ico-linkedin.svg" data-png-fallback="<?php bloginfo('template_url'); ?>/img/ico-linkedin.png" alt="LinkedIn"></a>
                <a href="<?php bloginfo('url'); ?>/blog" target="_blank"><img src="<?php bloginfo('template_url'); ?>/img/ico-blog.svg" data-png-fallback="<?php bloginfo('template_url'); ?>/img/ico-blog.png" alt="LinkedIn"></a>
            </div>
            <a href="#menu" class="mobile-nav mobile menu-link active">
                <span>Menu</span>
            </a>
        </div>
    </div> 
    
    <div class="nav-wrap">
        <div class="nav-outer-wrap">
            <div class="search-wrap dark-module">
                <div class="inner-wrap">
                    <?php get_search_form(); ?>   
                </div>
            </div>
            <div class="inner-wrap">
                <nav id="menu" class="menu clearfix" role="navigation">
                    <ul class="level-1">
                        <!-- <?php wp_nav_menu(array('menu' => 'Primary Nav','container' => '','items_wrap'      => '<ul class="level-1">%3$s</ul>',)); ?>   -->
                        
                        <li class="nav-1 has-subnav"><a href="#">Capabilities</a>
                            <?php wp_nav_menu(array('menu' => 'Capabilities','container' => '','items_wrap'      => '<ul class="sub-menu">%3$s</ul>',)); ?>
                        </li>
                        <li class="nav-2 has-subnav"><a href="#">Industries</a>
                            <?php wp_nav_menu(array('menu' => 'Industries','container' => '','items_wrap'      => '<ul class="sub-menu">%3$s</ul>',)); ?>
                        </li> 
                        <li class="nav-3 has-subnav"><a href="#">Corporate Links</a>
                            <?php wp_nav_menu(array('menu' => 'Corporate Links','container' => '','items_wrap'      => '<ul class="sub-menu">%3$s</ul>',)); ?>
                        </li> 
                        
                        <li class="nav-4 has-subnav"><a href="#">Portfolio</a>
                            <?php wp_nav_menu(array('menu' => 'Portfolio','container' => '','items_wrap'      => '<ul class="sub-menu">%3$s</ul>',)); ?>
                        </li> 
                        <li class="nav-5"><a href="<?php bloginfo('url'); ?>" class="site-logo"><img src="<?php bloginfo('template_url'); ?>/img/laserage-logo.png" alt="Site Logo"></a></li> 
                        
                        <li class="nav-6"><a href="<?php bloginfo('url'); ?>/about/resource-library/">Resources</a></li>
                        <li class="nav-7 has-subnav"><a href="#">About</a>
                            <?php wp_nav_menu(array('menu' => 'About','container' => '','items_wrap'      => '<ul class="sub-menu">%3$s</ul>',)); ?>
                        </li> 
                        <li class="nav-8"><a href="http://laserprocessing.laserage.com/contact-us-laserage" target="_blank">Contact</a></li> 
                        <li class="nav-9"><a href="http://laserprocessing.laserage.com/request-for-quote" target="_blank">Request Quote</a></li> 
                        <li class="nav-10"><a href="#" class="ico-search search-link">Search</a></li> 
                    </ul>
                </nav>
            </div>
        </div>
    </div>
    <div class="mobile-header">
        <div class="inner-wrap">
            <a href="<?php bloginfo('url'); ?>" class="site-logo"><img src="<?php bloginfo('template_url'); ?>/img/laserage-logo.png" alt="Site Logo"></a>
        </div>
    </div>
</header>

