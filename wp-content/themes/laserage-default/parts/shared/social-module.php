<section class="social-module dark-module">
                <div class="inner-wrap">
                    <h2 class="section-header">Latest Blog Posts</h2>
                    <div class="rows-of-3">
                    <?php global $query_string; $posts = query_posts('&order=ASC&orderby=date&order=DESC&showposts=3&post_type=post');?>        
        
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>


<article class="rssincl-entry">
          
            <h3 class="rssincl-itemtitle"><a href="<?php esc_url( the_permalink() ); ?>" title="Permalink to <?php the_title(); ?>" rel="bookmark"><?php the_title(); ?></a></h3>
            <p class="rssincl-itemdesc"><?php the_excerpt(); ?></p>

        </article>





<?php endwhile; ?>
<?php endif; ?>

<?php wp_reset_query(); ?>
</div>
                </div>
            </section>
        </section>
        
        
