<div class="img-txt-grid">
<a href="<?php bloginfo('url'); ?>/aerospace" class="img-txt-item-wrap <?php if (is_page( '995' )) : ?>img-txt-active<?php endif; ?>">
            <figure class="img-txt-figure"><img src="<?php bloginfo('template_url'); ?>/img/ico-aerospace.svg" alt="Aerospace Industry"></figure>
            <h3 class="img-txt-header">Aerospace</h3>
            
        </a>
        <a href="<?php bloginfo('url'); ?>/industrial" class="img-txt-item-wrap <?php if (is_page( '997' )) : ?>img-txt-active<?php endif; ?>">
            <figure class="img-txt-figure"><img src="<?php bloginfo('template_url'); ?>/img/ico-industrial.svg" alt="Industrial Industry" class="img-txt-image"></figure>
            <h3 class="img-txt-header">Industrial</h3>
            
        </a>
    <a href="<?php bloginfo('url'); ?>/medical" class="img-txt-item-wrap <?php if (is_page( '999' )) : ?>img-txt-active<?php endif; ?>">
            <figure class="img-txt-figure"><img src="<?php bloginfo('template_url'); ?>/img/ico-medical.svg" alt="Medical Industry"></figure>
            <h3 class="img-txt-header">Medical Components</h3>
    </a>
     
    <a href="<?php bloginfo('url'); ?>/microelectronics" class="img-txt-item-wrap <?php if (is_page( '1001' )) : ?>img-txt-active<?php endif; ?>">
            <figure class="img-txt-figure"><img src="<?php bloginfo('template_url'); ?>/img/ico-microelectronics.svg" alt="Microelectronics Industry" class="img-txt-image"></figure>
            <h3 class="img-txt-header">Microelectronics</h3>
            
        </a>
     
</div>