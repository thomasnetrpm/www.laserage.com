<section class="dark-module about-module">
                <div class="inner-wrap">
                    <h2 class="section-header">About Us</h2>
                    <div class="about-venta">
                        <figure><img src="<?php bloginfo('template_url'); ?>/img/logo-ventamedical.svg" alt="Venta Medical"></figure>
                        <p>
                           Venta Medical is a medical device contract manufacturer, providing comprehensive manufacturing services to medical and life science companies. In 2013, Laserage Technology Corporation acquired Venta Medical in order to offer more comprehensive services and establish a California presence that has been key in serving the medical technology industry.
                        </p>
                    </div>
                    <div class="about-laserage">
                        <figure><img src="<?php bloginfo('template_url'); ?>/img/logo-laserage.svg" alt="Laserage"></figure>
                        <p>
                           Laserage is a recognized leader in the laser processing industry. Through the use of our custom-designed CO2, Nd:YAG, fiber, disk and Femto laser systems, we offer a wide variety of laser processing services and are equipped to handle small- to high-volume production.
                        </p>
                    </div>
                    <!--<p class="text-aligncenter clearboth"><a href="news-events/" class="gray-btn">See What's New</a></p>-->
                </div>
            </section> <!-- about-module END -->