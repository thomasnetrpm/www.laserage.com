<section class="img-txt-module">
                <div class="inner-wrap">
                    <h2 class="section-header">Capabilities</h2>
                    <div class="img-txt-grid">
                    	<div class="img-txt-item-wrap">
                            <div class="img-text-link-lightbox">
                                <a href="<?php bloginfo('url'); ?>/precision-tube-and-finishing" class="img-text-link">Link</a>
                                <a href="<?php bloginfo('url'); ?>/assets/precision-tube-cutting-v2.jpg" class="img-text-lightbox lightbox">Popup</a>
                            </div>
                            <a href="<?php bloginfo('url'); ?>/precision-tube-and-finishing" class="img-txt-item-inverted">
                                <figure class="img-txt-figure"><img src="<?php bloginfo('url'); ?>/assets/thumb-precision-tube-cutting.jpg" alt=""></figure>
                                <h3 class="img-txt-header">Precision Tube <br>Cutting</h3>
                                <p class="img-txt-body">
                                   Our precision tube cutting & finishing capabilities allow us to create laser-cut tubular components for a wide variety of medical applications, including implants, delivery systems & instrumentation. 
                                </p>
                                <span class="img-txt-btn"><b class="yellow-outline-btn">Learn More</b></span>
                            </a>
                        </div>
                        <div class="img-txt-item-wrap">
                             <div class="img-text-link-lightbox">
                                <a href="<?php bloginfo('url'); ?>/laser-cutting-welding" class="img-text-link">Link</a>
                                <a href="<?php bloginfo('url'); ?>/assets/laser-cutting-welding-img.jpg" class="img-text-lightbox lightbox">Popup</a>
                            </div>
                            <a href="<?php bloginfo('url'); ?>/laser-cutting-welding" class="img-txt-item-inverted">
                                <figure class="img-txt-figure"><img src="<?php bloginfo('url'); ?>/assets/thumb-laser-cutting-welding.jpg" alt=""></figure>
                                <h3 class="img-txt-header">Laser Cutting<br> & Welding</h3>
                                <p class="img-txt-body">
    Our commitment to excellence combined with more than 35 years of service allows us to provide high-quality laser-cut products. We specialize in laser-cut 2D flat parts & customized 3D pre-fabricated components.
                                </p>
                                <span class="img-txt-btn"><b class="yellow-outline-btn">Learn More</b></span>
                            </a>
                        </div>
                        <div class="img-txt-item-wrap">
                            <div class="img-text-link-lightbox">
                                <a href="<?php bloginfo('url'); ?>/ceramic-substrates" class="img-text-link">Link</a>
                                <a href="<?php bloginfo('url'); ?>/assets/Ceramic-Boards-5x7-1024x799.jpg" class="img-text-lightbox lightbox">Popup</a>
                            </div>
                            <a href="<?php bloginfo('url'); ?>/ceramic-substrates" class="img-txt-item-inverted">
                                <figure class="img-txt-figure"><img src="<?php bloginfo('url'); ?>/assets/thumb-ceramic-substrates.jpg" alt="" class="img-txt-image"></figure>
                                <h3 class="img-txt-header">Ceramic <br>Substrates</h3>
                                <p class="img-txt-body">
    We offer a number of ceramic substrate processing services to manufacture high-quality products for microelectronic applications. For tight tolerances, precise drilling & delicate cuts, you can depend on Laserage.                            </p>
                                <span class="img-txt-btn"><b class="yellow-outline-btn">Learn More</b></span>
                            </a>
                        </div>
                    	<div class="img-txt-item-wrap">
                            <div class="img-text-link-lightbox">
                                <a href="<?php bloginfo('url'); ?>/custom-assemblies/" class="img-text-link">Link</a>
                                <a href="<?php bloginfo('url'); ?>/assets/custom-assembly-1024x768.jpg" class="img-text-lightbox lightbox">Popup</a>
                            </div>
                            <a href="<?php bloginfo('url'); ?>/custom-assemblies/" class="img-txt-item-inverted">
                                <figure class="img-txt-figure"><img src="<?php bloginfo('url'); ?>/assets/thumb-custom-assemblies.jpg" alt="Custom Assemblies" class="img-txt-image"></figure>
                                <h3 class="img-txt-header">Custom<br>Assemblies</h3>
                                <p class="img-txt-body">
    Laserage offers extensive custom cable assembly services. From prototypes to large volume orders, we can develop interconnection assemblies that meet your exact needs—quickly & at a competitive price.   

                                </p>
                                <span class="img-txt-btn"><b class="yellow-outline-btn">Learn More</b></span>
                            </a>
                        </div>
                    </div>
                </div>
            </section> <!-- img-txt-module END -->