<!--Secondary Content-->
<aside class="site-content-secondary">

<?php if (is_page( '937' ) || '937' == $post->post_parent) : ?>
      
    <h3 class="nav-aside-header">About Laserage</h3>
    <nav class="nav-aside">
         <?php wp_nav_menu(array('menu' => 'About','container' => '','items_wrap'      => '<ul>%3$s</ul>',)); ?>        
    </nav>
    
<?php elseif (is_page( '949' ) || '949' == $post->post_parent) : ?>
    <h3 class="nav-aside-header">Capabilities</h3>
    <nav class="nav-aside">
         <?php wp_nav_menu(array('menu' => 'Capabilities','container' => '','items_wrap'      => '<ul>%3$s</ul>',)); ?>        
    </nav>
    
<?php elseif (is_page( '1011' ) || '1011' == $post->post_parent) : ?>
    <h3 class="nav-aside-header">Other Portfolios</h3>
    <nav class="nav-aside">
         <?php wp_nav_menu(array('menu' => 'Portfolio','container' => '','items_wrap'      => '<ul>%3$s</ul>',)); ?>        
    </nav>
    
<?php elseif (is_page( '993' ) || '993' == $post->post_parent) : ?>
    <h3 class="nav-aside-header">Other Industries</h3>
    <nav class="nav-aside">
         <?php wp_nav_menu(array('menu' => 'Industries','container' => '','items_wrap'      => '<ul>%3$s</ul>',)); ?>        
    </nav>
    
<?php elseif (is_page( '1581' ) || '1581' == $post->post_parent) : ?>
    <h3 class="nav-aside-header">中国</h3>
    <nav class="nav-aside">
         <?php wp_nav_menu(array('menu' => 'Chinese','container' => '','items_wrap'      => '<ul>%3$s</ul>',)); ?>        
    </nav>
    
<?php endif; ?>

     
    <a href="http://laserprocessing.laserage.com/request-for-quote" class="orange-btn ico-rfq site-intro-cta-3" target="_blank"><span>Request for Quote</span></a>
    
    <?php if(get_field('aside_cta') ): ?>
      <div class="cta-aside">
        <?php the_field('aside_cta'); ?>
     </div>   
    
                   
    <?php endif; ?>
    
    
<?php if(get_field('additional_aside_content') ): ?>
		<?php the_field('additional_aside_content'); ?>
<?php endif; ?>
</aside>


