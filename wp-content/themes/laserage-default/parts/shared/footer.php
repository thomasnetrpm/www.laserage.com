

<!--Site Footer-->
<footer class="site-footer" role="contentinfo">
    <div class="site-footer-top dark-module">
        <div class="inner-wrap">
            <p>&copy; <?php echo date("Y"); ?> <?php bloginfo( 'name' ); ?> <br> 3021 North Delany Road &nbsp;| &nbsp;Waukegan, IL 60087 &nbsp;| &nbsp;847.249.5900<br>1971 Milmont Drive<span> &nbsp;| &nbsp;Milpitas, CA 95035 &nbsp;| &nbsp;408.770.8520<br></span><a href="https://mail.google.com/mail/?view=cm&amp;fs=1&amp;tf=1&amp;to=Sales@LaserAge.com" target="_blank">Sales@Laserage.com</a></p>
            <div class="social-wrap">
                <a href="https://www.facebook.com/laseragetechnologycorp" target="_blank"><span><img src="<?php bloginfo('template_url'); ?>/img/ico-facebook.svg" data-png-fallback="<?php bloginfo('template_url'); ?>/img/ico-facebook.png" alt="Facebook"></span></a>
                <a href="https://twitter.com/laseragetech" target="_blank"><span><img src="<?php bloginfo('template_url'); ?>/img/ico-twitter.svg" data-png-fallback="<?php bloginfo('template_url'); ?>/img/ico-twitter.png" alt="Twitter"></span></a>
                <a href="https://plus.google.com/u/0/+Laserage/posts" target="_blank"><span><img src="<?php bloginfo('template_url'); ?>/img/ico-googleplus.svg" data-png-fallback="<?php bloginfo('template_url'); ?>/img/ico-googleplus.png" alt="Google Plus"></span></a>
                <a href="https://www.linkedin.com/company/133282" target="_blank"><span><img src="<?php bloginfo('template_url'); ?>/img/ico-linkedin.svg" data-png-fallback="<?php bloginfo('template_url'); ?>/img/ico-linkedin.png" alt="LinkedIn"></span></a>
                <a href="http://laserprocessing.laserage.com/blog" target="_blank"><span><img src="<?php bloginfo('template_url'); ?>/img/ico-blog.svg" data-png-fallback="<?php bloginfo('template_url'); ?>/img/ico-blog.png" alt="LinkedIn"></span></a>
            </div>
        </div>
    </div>
     <div class="site-footer-bottom">
        <div class="inner-wrap">
            <p>Site Created by <a href="http://business.thomasnet.com/rpm">ThomasNet RPM</a>   |   <a href="<?php bloginfo('url'); ?>/privacy">Privacy</a>   |   <a href="<?php bloginfo('url'); ?>/sitemap.html">Sitemap</a></p>
        </div>
    </div>
</footer>